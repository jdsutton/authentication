from authentication.util.testing.MockDBClient import MockDBClient
from eekquery.ConfigFactory import ConfigFactory
from eekquery.DataObject import DataObject
from eekquery.test.MockDBClientFactory import MockDBClientFactory
import json
from mockutils.MockUtils import MockUtils
from mockutils.IsolatedTest import IsolatedTest
import unittest

class test_Authentication(IsolatedTest):

    @classmethod
    def setUpClass(cls):
        global Authentication, Authenticator, requests

        super().setUpClass()

        from authentication.util.testing.MockRequests import MockRequests
        MockUtils.mockModule('requests', MockRequests)

        from authentication.Authentication import Authentication
        from authentication.Authenticator import Authenticator
        import requests

    @classmethod
    def getDBClient(cls):
        config = ConfigFactory \
            .setEnvironment(ConfigFactory.ENV.TEST) \
            .produce('config/test.json')
        
        return MockDBClientFactory.produce(config)

    @classmethod
    def getAuth(cls):
        return Authentication(cls.getDBClient()) \
            .configure(DataObject({
                'dbClient': None,
                'linkedinClientId': 'fake-client-id-LI',
                'linkedinRedirectURI': 'fake-redirect-uri-LI',
                'linkedinClientSecret': 'fake-client-secret-LI',
            }, autoField=True))

    @classmethod
    def getAuthentiator(cls):
        return Authenticator(DataObject({
            'dbClient': cls.getDBClient(),
        }, autoField=True))

    def test_getAuthorizeURL(self):
        auth = test_Authentication.getAuth()

        result = auth.getAuthorizeURL(provider=Authenticator.PROVIDER.LINKEDIN)

        start = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=fake-client-id-LI&redirect_uri=fake-redirect-uri-LI'
        self.assertTrue(result.startswith(start))

    def test_exchange_refreshAccessToken(self):
        provider = Authenticator.PROVIDER.LINKEDIN
        auth = test_Authentication.getAuth()
        uri = auth.getAuthorizeURL(provider=provider)

        requests.setResponse({
            'id': 12345,
            'access_token': 'fake-access-token-LI',
            'expires_in': 60 * 24 * 60 * 60, # 60 days.
            'refresh_token': 'fake-refresh-token',
        })

        state = test_Authentication.getAuthentiator()._getState()
        credentials = auth.exchange(
            'fake-code', 
            state=state,
            provider=provider,
        )
        userId = credentials.id

        self.assertEqual(credentials.accessToken, 'fake-access-token-LI')

        # TODO: Assert fake state throws error.

        requests.setResponse({
            'access_token': 'fake-access-token-LI-2`',
            'expires_in': 60 * 24 * 60 * 60, # 60 days.
            'refresh_token': 'fake-refresh-token-2',
        })

        auth.refreshAccessToken(credentials.asDict(), provider=provider)

        # Assert if user logs on again from another domain they get the same user ID.
        requests.setResponse({
            'id': 12345,
            'access_token': 'fake-access-token-LI-2',
            'expires_in': 60 * 24 * 60 * 60, # 60 days.
            'refresh_token': 'fake-refresh-token-2',
        })
        state = test_Authentication.getAuthentiator()._getState()
        credentials = auth.exchange(
            'fake-code',
            state=state,
            provider=provider,
            domain='google.com',
        )

        self.assertEqual(credentials.id, userId)

if __name__ == '__main__':
    unittest.main()