from authentication.Authenticator import Authenticator
from authentication.AuthenticatorFactory import AuthenticatorFactory
from authentication.Credentials import Credentials
from authentication.permissions.HasValidCredentialsPermission import HasValidCredentialsPermission
from eekquery.RequiresPermission import RequiresPermission
from eekquery.SQLDate import SQLDate
from py3typing.Typing import *
import secrets

class Authentication:
    '''
    https://developers.google.com/api-client-library/python/guide/aaa_oauth
    '''

    _API_KEY_SIZE = 64

    def __init__(self, dbClient):
        self._dbClient = dbClient
        self._config = None

    def configure(self, config):
        '''
        Sets up configuration details.
        '''

        self._config = config
        self._config.dbClient = self._dbClient

        return self

    @staticmethod
    def getUserByAPIKey(self, apiKey):
        return self._dbClient.getRow(Credentials.TABLE, {
            'apiKey': apiKey,
        }, offset=0, limit=1, orderBy=None)

    @Params(object)
    @Returns(str)
    def getAuthorizeURL(self, *, provider=Authenticator.PROVIDER.GOOGLE):
        '''
        Returns a URL to redirect the user to for authorization.
        '''

        return AuthenticatorFactory.produce(self._config, provider).getAuthorizeURL()

    def _getNewUsername(self):
        # TODO.
        return 'user{}'.format(secrets.token_urlsafe()[:Authentication._API_KEY_SIZE])

    @Params(object, str)
    @Returns(Credentials)
    def exchange(self, code, *, state='', domain='', provider=Authenticator.PROVIDER.GOOGLE):
        '''
        Exchange an authorization code for a Credentials object.
        '''

        auth = AuthenticatorFactory.produce(self._config, provider)

        credentials = auth.exchange(code, state)

        oauth2Id = auth.getOauth2Id(credentials)

        user = Credentials({
            **credentials.asDict(),
            'domain': domain,
            'oauth2Id': oauth2Id,
            'oauth2Provider': provider,
        })

        user.expiresAt = str(SQLDate(user.expiresAt))

        userRow = self._dbClient.getRow(
            user.TABLE,
            {
                'oauth2Id': oauth2Id,
                'oauth2Provider': provider,
            },
            orderBy=None,
        )
    
        if userRow is not None:
            # Associate new login with same user ID.
            user.id = userRow['id']
        else:
            # If no ID, create a new user row.
            user.username = self._getNewUsername()

        # Write new credentials.
        self._dbClient.writeRow(
            user.asTableRow(user.TABLE),
            user.TABLE,
        )
        
        return user.read(self._dbClient)

    @Params(object, dict)
    @Returns(Credentials)
    def refreshAccessToken(self, user, *, provider=Authenticator.PROVIDER.GOOGLE):
        '''
        Attempts to refresh the given access token and returns an updated Credentials.
        '''

        provider = AuthenticatorFactory.produce(self._config, provider)

        user = provider.refreshAccessToken(Credentials(user))

        user.expiresAt = str(SQLDate(user.expiresAt))

        self._dbClient.writeRow(
            user.asTableRow(user.TABLE),
            user.TABLE,
        )

        return user.read(self._dbClient)

    @Params(object, dict)
    @RequiresPermission(HasValidCredentialsPermission())
    def writeUserDetails(self, user, **kwargs):
        '''
        Updates the user's username in the DB.
        '''

        user = Credentials({
            'id': user['id'],
            'username': user['username'],
        }).update(self._dbClient)

    def recordUserConsent(self, user, **kwargs):
        # TODO.

        pass

    @Params(object, dict)
    @RequiresPermission(HasValidCredentialsPermission())
    def generateAPIKey(self, *, user):
        # TODO: Lock table.

        while True:
            apiKey = secrets.token_urlsafe()[:Authentication._API_KEY_SIZE]

            row = self._dbClient.getRow(Credentials.TABLE, {
                'apiKey': apiKey,
            })

            if row is None:
                self._dbClient.writeRow({
                    'oauth2Id': user['oauth2Id'],
                    'oauth2Provider': user['oauth2Provider'],
                    'apiKey': apiKey,
                }, Credentials.TABLE)

                return apiKey
