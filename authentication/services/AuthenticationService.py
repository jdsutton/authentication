from pythonlambdaserver.LambdaService import LambdaService
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig
from eekquery.ConfigFactory import ConfigFactory
from urllib.parse import urljoin

class AuthenticationService(LambdaService):
    '''
    TODO.
    '''

    METHOD_CONFIGURATION = {
        'authentication': {
            'getAuthorizeURL': LambdaServiceMethodConfig(True),
            'getUserProfile': LambdaServiceMethodConfig(True),
            'exchange': LambdaServiceMethodConfig(True),
            'refreshAccessToken': LambdaServiceMethodConfig(True),
            'writeUserDetails': LambdaServiceMethodConfig(True),
            'recordUserConsent': LambdaServiceMethodConfig(True),
            'generateAPIKey': LambdaServiceMethodConfig(True),
        },
    }

    def _getDBClient(self):
        raise RuntimeError('Not Implemented')

    @property
    def authentication(self):
        from authentication.Authentication import Authentication

        configPath = 'config/{}.config.json'.format(LambdaService.context['lambdaName'])
        ConfigFactory.setEnvironment(ConfigFactory.ENV.PROD)
        config = ConfigFactory.produce(configPath)
        client = self._getDBClient()
        context = LambdaService.context
        origin = context.get('Origin', context.get('origin', None))

        if config.hasPropertyNotNone('googleRedirectURI'):
            uri = config.googleRedirectURI

            if uri.startswith('/'):
                config.googleRedirectURI = urljoin(origin, uri)

        result = Authentication(client)
        result.configure(config)

        return result
