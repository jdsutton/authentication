from authentication.Authenticator import Authenticator
from authentication.Credentials import Credentials
from datetime import datetime, timedelta
from py3typing.Typing import *
import json
import requests
import urllib

class LinkedinAuthenticator(Authenticator):
    '''
    TODO.
    '''

    _URI = 'https://www.linkedin.com/oauth/v2/authorization?{}'
    _EXCHANGE_URI = 'https://www.linkedin.com/oauth/v2/accessToken'
    # v2 api
    # _PROFILE_URI = 'https://api.linkedin.com/v2/me'
    _PROFILE_URI = 'https://api.linkedin.com/v2/me'
    _PROFILE_SCOPE = 'r_liteprofile r_emailaddress'

    def __init__(self, config):
        super().__init__(config)

        self._redirectURI = config.linkedinRedirectURI
        self._clientSecret = config.linkedinClientSecret
        self._clientId = config.linkedinClientId

    @Params(object)
    @Returns(str)
    def getAuthorizeURL(self):
        '''
        Returns a URL to redirect the user to for authorization.
        '''
        data = {
            'response_type': 'code',
            'client_id': self._clientId,
            'redirect_uri': self._redirectURI,
            'state': self._getState(),
            'scope': LinkedinAuthenticator._PROFILE_SCOPE,
        }
        
        return LinkedinAuthenticator._URI.format(urllib.parse.urlencode(data))

    @Params(object, str)
    @Returns(Credentials)
    def exchange(self, code, state):
        '''
        Exchange an authorization code for a Credentials object.
        '''

        super().exchange(code, state)

        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': self._redirectURI,
            'client_id': self._clientId,
            'client_secret': self._clientSecret,
            'scope': LinkedinAuthenticator._PROFILE_SCOPE,
        }

        response = requests.post(
            LinkedinAuthenticator._EXCHANGE_URI,
            data=data,
        )
        response = response.json()

        exp = datetime.now() + timedelta(
            seconds=response['expires_in'])

        return Credentials({
            'accessToken': response['access_token'],
            'refreshToken': response.get('refresh_token', None),
            'expiresAt': exp.isoformat() + 'Z',
        })

    @Params(object, dict)
    @Returns(Credentials)
    def refreshAccessToken(self, user):
        '''
        Attempts to refresh the given access token and returns an updated Credentials.
        '''

        super().refreshAccessToken(user)

        response = requests.post(LinkedinAuthenticator._EXCHANGE_URI, {
            'grant_type': 'refresh_token',
            'refresh_token': user.get('refreshToken', None),
            'client_id': self._clientId,
            'client_secret': self._clientSecret,
        }).json()

        exp = datetime.now() + timedelta(
            seconds=response['expires_in'])

        user = Credentials(user)

        user.accessToken = response['access_token']
        user.refreshToken = response['refresh_token']
        user.expiresAt = str(exp.isoformat()) + 'Z'

        return user

    @Params(object, dict)
    @Returns(str)
    def getOauth2Id(self, credentials):
        '''
        Returns an ID for the given user.
        '''

        uri = LinkedinAuthenticator._PROFILE_URI
        response = requests.get(uri, headers={
            'Authorization': 'Bearer {}'.format(credentials.get('accessToken', None)),
        })
        response = response.json()

        try:
            return response['id']
        except Exception as e:
            raise RuntimeError(response)