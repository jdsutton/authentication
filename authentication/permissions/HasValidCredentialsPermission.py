from authentication.Credentials import Credentials

class HasValidCredentialsPermission:
    '''
    Permission to check the given credentials are valid.
    '''

    def __init__(self, dbClient=None):
        self._dbClient = dbClient
        self._err = 'Invalid credentials'

    def __str__(self):
        return self._err

    def _withUser(self, user, dbClient):
        user = Credentials(user)
        userRow = user.read(self._dbClient, byKeysOnly=True)

        if userRow is None:
            self._err = 'User not found'

            return False

        if user.expired():
            self._err = 'Credentials expired'

            return False

        return True

    def _withAPIKey(self, apiKey, dbClient):
        userRow = dbClient.getRow(Credentials.TABLE, {
            'apiKey': apiKey,
        }, offset=0, limit=1, orderBy=None)

        if Credentials(userRow).expired(): return False

        return userRow is not None

    def __call__(self, *args, **kwargs):
        user = kwargs.get('user', None)
        apiKey = kwargs.get('apiKey', None)
        dbClient = self._dbClient or args[0]._dbClient

        if user is not None:
            return self._withUser(user, dbClient)
        elif apiKey is not None:
            return self._withAPIKey(apiKey, dbClient)

        return False
