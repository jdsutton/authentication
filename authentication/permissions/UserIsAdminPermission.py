from authentication.Credentials import Credentials

class UserIsAdminPermission:
    '''
    Test if the given user is an admin.
    Does not test credentials.
    '''

    def _lookupUserFromAPIKey(apiKey, dbClient):
        return Credentials({
            'apiKey': apiKey,
        }, byKeysOnly=False).read(dbClient)

    def __call__(self, *args, **kwargs):
        # db = Environment.getMysqlClient()
        user = kwargs.get('user', None)
        apiKey = kwargs.get('apiKey', None)

        if user is None:
            if apiKey is None:
                return False
            
            user = self._lookupUserFromAPIKey(apiKey)
        
        user = Credentials(user).asDict()

        userRow = db.getRow(Credentials.TABLE, {
            'oauth2Id': user['oauth2Id'],
            'oauth2Provider': user['oauth2Provider'],
            'isAdmin': 1,
        }, offset=0, limit=1, orderBy=None)

        return userRow is not None
