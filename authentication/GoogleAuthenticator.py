from authentication.Authenticator import Authenticator
from authentication.Credentials import Credentials
from eekquery.SQLDate import SQLDate
import httplib2
import json
from oauth2client.client import GoogleCredentials
from oauth2client.client import OAuth2WebServerFlow
from py3typing.Typing import *

class GoogleAuthenticator(Authenticator):
    '''
    TODO.
    '''

    class _SCOPE:
        GOOGLE_USER_PROFILE = 'https://www.googleapis.com/auth/userinfo.profile'

    _GOOGLE_TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    # Endpoint for retrieving user profile details.
    _USER_PROFILE_ENDPOINT = 'https://www.googleapis.com/oauth2/v1/userinfo'
    _USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'

    def __init__(self, config):
        super().__init__(config)

        self._config = config
        
        self._flow = OAuth2WebServerFlow(
            client_id=config.googleClientId,
            client_secret=config.googleClientSecret,
            scope=GoogleAuthenticator._SCOPE.GOOGLE_USER_PROFILE,
            redirect_uri=config.googleRedirectURI,
            prompt='consent')

    def _getGoogleCredentialsFromCredentials(self, credentials):
        '''
        TODO.
        '''

        expiresAt = SQLDate(credentials.expiresAt).get()
        expiresAt = expiresAt.replace(tzinfo=None)

        return GoogleCredentials(
            credentials.accessToken,
            self._config.googleClientId,
            self._config.googleClientSecret,
            credentials.refreshToken,
            expiresAt,
            GoogleAuthenticator._GOOGLE_TOKEN_URI,
            GoogleAuthenticator._USER_AGENT,
        )

    def _getHttp(self, credentials):
        '''
        TODO.
        '''

        http = httplib2.Http()
        credentials.authorize(http)

        return http

    @Params(object)
    @Returns(str)
    def getAuthorizeURL(self):
        '''
        Returns a URL to redirect the user to for authorization.
        '''

        url = self._flow.step1_get_authorize_url()

        return url + '&state=' + self._getState()

    @Params(object, str)
    @Returns(Credentials)
    def exchange(self, code, state):
        '''
        Exchange an authorization code for a Credentials object.
        '''

        super().exchange(code, state)

        credentials = self._flow.step2_exchange(code)
        exp = credentials.token_expiry

        return Credentials({
            'accessToken': credentials.access_token,
            'refreshToken': credentials.refresh_token,
            'expiresAt': exp.isoformat() + 'Z',
        })

    @Params(object, Credentials)
    @Returns(Credentials)
    def refreshAccessToken(self, user):
        '''
        Attempts to refresh the given access token and returns an updated Credentials.
        '''

        super().refreshAccessToken(user)

        googleCredentials = self._getGoogleCredentialsFromCredentials(user)
        http = self._getHttp(googleCredentials)

        if googleCredentials.access_token_expired:
            googleCredentials.refresh(http)

        user.accessToken = googleCredentials.access_token
        user.refreshToken = googleCredentials.refresh_token
        user.expiresAt = str(SQLDate(googleCredentials.token_expiry))

        return user

    @Params(object, Credentials)
    @Returns(str)
    def getOauth2Id(self, credentials):
        '''
        Retursn an ID for the given user.
        '''

        googleCredentials = self._getGoogleCredentialsFromCredentials(credentials)

        http = self._getHttp(googleCredentials)
        uri = GoogleAuthenticator._USER_PROFILE_ENDPOINT + '?fields=id'

        resp, content = http.request(uri)

        if resp.status != 200:
            Environment.getLogger().error('Failed to lookup user profile: {}\n{}'.format(resp, content))

        content = json.loads(content.decode())

        return content['id']
