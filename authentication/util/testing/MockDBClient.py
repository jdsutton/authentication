class MockDBClient:
    '''
    TODO.
    '''

    _result = None
    _rowsWritten = []

    def writeRow(self, row, *args, **kwargs):
        MockDBClient._rowsWritten.append(row)

    @classmethod
    def getRowsWritten(cls):
        return cls._rowsWritten

    def getRow(self, *args, **kwargs):
        return MockDBClient._result

    @classmethod
    def setResult(cls, result):
        cls._result = result

        return self