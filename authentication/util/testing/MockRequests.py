class MockRequests:
    '''
    TODO.
    '''

    _response = None

    class Response:

        def __init__(self, data):
            self._data = data

        def json(self):
            return self._data

    @classmethod
    def setResponse(cls, response):
        cls._response = response

    @classmethod
    def post(cls, *args, **kwargs):
        return MockRequests.Response(cls._response)

    @classmethod
    def get(cls, *args, **kwargs):
        return MockRequests.Response(cls._response)