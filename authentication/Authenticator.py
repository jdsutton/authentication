from authentication.Credentials import Credentials
from py3typing.Typing import *
from random import choice
from string import ascii_uppercase

class Authenticator:
    '''
    TODO.
    '''

    class PROVIDER:
        '''
        OAuth2 providers.
        '''

        GOOGLE = 'google'
        LINKEDIN = 'linkedin'

    _STATE_SIZE = 16
    _STATE_TOKEN_TABLE = 'StateTokens'

    def __init__(self, config):
        self._dbClient = config.dbClient

    @Params(object)
    @Returns(str)
    def _getState(self):
        '''
        Returns a hard-to-guess state value for preventing CSRF.
        '''

        stateToken = ''.join(choice(ascii_uppercase) for i in range(Authenticator._STATE_SIZE))

        self._dbClient.writeRow({
            'stateToken': stateToken,
        }, Authenticator._STATE_TOKEN_TABLE)

        return stateToken

    @Params(object)
    @Returns(str)
    def getAuthorizeURL(self):
        '''
        Returns a URL to redirect the user to for authorization.
        '''

        pass

    @Params(object, str)
    @Returns(Credentials)
    def exchange(self, code, state):
        '''
        Exchange an authorization code for a Credentials object.
        '''

        self._assertStateMatches(state)

    def _assertStateMatches(self, state):
        '''
        Assert state token is in DB.
        '''

        row = self._dbClient.getRow(Authenticator._STATE_TOKEN_TABLE, {
            'stateToken': state,
        }, orderBy=None)

        assert(row is not None)

        self._dbClient.deleteRows(Authenticator._STATE_TOKEN_TABLE, {
            'stateToken': state,
        }, limit=1)

    @Params(object, Credentials)
    @Returns(Credentials)
    def refreshAccessToken(self, user):
        '''
        Attempts to refresh the given access token and returns an updated Credentials.
        '''

        # The service must ensure the refresh token in the request was issued to the authenticated client.
        userRow = user.read(self._dbClient, byKeysOnly=True)
        assert(userRow.refreshToken == user.refreshToken) 

    @Params(object, dict)
    @Returns(str)
    def getOauth2Id(self, credentials):
        '''
        Retursn an ID for the given user.
        '''

        pass