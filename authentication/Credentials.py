from datetime import datetime, timezone
from eekquery.DataObject import DataObject
from eekquery.SQLDate import SQLDate

class Credentials(DataObject):
    '''
    User login credentials.
    '''

    TABLE = 'Users'

    def __init__(self, fromDict):
        super().__init__(fromDict)
        
        self.forTable(self.__class__.TABLE)
        self.addField('id')
        self.addField('oauth2Id', isKey=True)
        self.addField('oauth2Provider', isKey=True)
        self.addField('domain')
        self.addField('accessToken')
        self.addField('refreshToken')
        self.addField('expiresAt')
        self.addField('username')
        self.addField('isAdmin')
        self.addField('apiKey')
        
        if not self.hasPropertyNotNone('domain'):
            self.domain = ''

    def expired(self):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)

        return now >= SQLDate(self.expiresAt).get()

    def getDetails(self):
        return Credentials({
            'id': self.id,
            'username': self.username,
        })

    def read(self, dbClient, *, byKeysOnly=True, **kwargs):
        table = self.__class__.TABLE

        if byKeysOnly:
            keys = self.getKeys()
        else:
            keys = self.asDict()

        result = dbClient.getRow(table, keys, offset=0, limit=1, orderBy=None)

        if result is None: return None

        return Credentials(result)
