from authentication.Authenticator import Authenticator
from authentication.GoogleAuthenticator import GoogleAuthenticator
from authentication.LinkedinAuthenticator import LinkedinAuthenticator
from py3typing.Typing import *

class AuthenticatorFactory:
    '''
    TODO.
    '''

    class UnrecognizedProviderError(RuntimeError):
        pass

    @classmethod
    @Params(dict, str)
    @Returns(Authenticator)
    def produce(cls, config, provider):
        '''
        TODO.
        '''

        if provider == Authenticator.PROVIDER.GOOGLE:
            return GoogleAuthenticator(config)
        elif provider == Authenticator.PROVIDER.LINKEDIN:
            return LinkedinAuthenticator(config)
        else:
            raise AuthenticatorFactory.UnrecognizedProviderError(provider)
