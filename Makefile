PYTHON=$(shell which python3.6)
DB_NAME=Authentication

install:
	$(SUDO) apt-get update
	
	pip install --upgrade .

test:
	$(PYTHON) -m nose --nocapture --with-coverage --cover-package=authentication --cover-erase
	coverage report -m --skip-covered --omit=env || true

