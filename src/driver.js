import('Authentication.js');
import('/src/JSUtils/util/Dom.js');

function setCredentials(user) {
    const nextURL = localStorage.getItem('nextURL');

    localStorage.setItem('user', JSON.stringify(user));
    window.location = nextURL || '/';
}

function requestCookies(credentials) {
    const modal = new CookieConsentModalComponent();

    modal.onConsent(() => {
        Authentication.recordCookieOptIn();
        setCredentials(credentials);
    });

    modal.onDecline(() => {
        Authentication.recordCookieOptOut();
        window.location = '/';
    });

    Dom.appendTo(document.body, modal.render());
}

function main() {
    const q = QueryString.getAsObj();
    const code = q.code;
    const state = q.state;

    Authentication.exchange(code, state).then(user => {
        Authentication.setUser(user);

        if (!Authentication.canStoreCookies()) {
            requestCookies(user);
        } else {
            setCredentials(user);
        }
    });
}

window.onload = () => {
    main();
}