/**
 * @class
 */
class AuthenticationProviderSelectionModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor() {
        const params = {};
        
        super(AuthenticationProviderSelectionModalComponent.ID.TEMPLATE.THIS, params);
    
        this._onProviderSelectedCallbacks = [];
    }

    /**
     * @public
     */
    setOnProviderSelected(callback) {
        this._onProviderSelectedCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    onLoginButtonClicked(provider) {
        this._onProviderSelectedCallbacks.forEach(callback => {
            callback(provider);
        });
    }
}

AuthenticationProviderSelectionModalComponent.ID = {
    TEMPLATE: {
        THIS: 'AuthenticationProviderSelectionModalComponent_template',
    },
};
