/**
 * @class
 */
class UserSettingsModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor() {
        const user = Authentication.getUser();
        const params = {
            username: user.username,
        };
        
        super(UserSettingsModalComponent.ID.TEMPLATE.THIS, params);
    
        this._apiKey = null;
    }

    /**
     * @public
     */
    onGenerateAPIKeyButtonClicked() {
        Authentication.generateAPIKey().then(key => {
            let input = this.getElement(this._ids().API_KEY);

            input.value = key;
        });
    }

    /**
     * @public
     */
    onSetUsernameButtonClicked() {
        let user = Authentication.getUser();
        let button = this.getElement(this._ids().SET_USERNAME_BUTTON);
        let message = this.getElement(this._ids().USERNAME_MESSAGE);
        const data = Dom.getDataFromForm(this.getId());

        button.disabled = true;
        user.username = data.username;

        const finish = s => {
            button.disabled = false;

            Dom.setContents(message, s);

            setTimeout(() => {
                Dom.setContents(message, '');
            }, 1500);
        }

        Authentication.writeUserDetails(user).then(() => {
            finish('Username set!');

        }).catch(err => {
            finish('Something went wrong, please try again.');
        });
    }
}

UserSettingsModalComponent.ID = {
    API_KEY: '_apiKey',
    SET_USERNAME_BUTTON: '_setUsernameButton',
    TEMPLATE: {
        THIS: 'UserSettingsModalComponent_template',
    },
    USERNAME_MESSAGE: '_usernameMessage',
};
