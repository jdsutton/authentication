import('/src/JSUtils/backend/BackendService.js');
import('/src/JSUtils/util/Datetime.js');
import('/src/JSUtils/util/Dom.js');

/**
 * @class
 * http://oauth2client.readthedocs.io/en/latest/source/oauth2client.client.html#oauth2client.client.OAuth2Credentials
 */
class Authentication {

    /**
     * @public
     */
    static logInIfNeeded() {
        return this.isLoggedIn().then(result => {
            if (!result) {
                this.logIn();

                return Promise.reject();
            }

            return Promise.resolve();
        });
    }

    /**
     * @public
     */
    static exchange(code, state) {
        const domain = window.location.hostname;
        const provider = this.getProvider();

        return this._SERVICE.invoke('exchange', [code], {
            state,
            provider,
            domain,
        },/* unsafe */ true);
    }

    /**
     * @public
     */
    static generateAPIKey() {
        const user = this.getUser();

        return this._SERVICE.invoke('generateAPIKey', [], {user});
    }

    /**
     * @public
     */
    static writeUserDetails(userDetails) {
        return this._SERVICE.invoke('writeUserDetails', [userDetails], {}).then(result => {
            let user = this.getUser();

            user.username = userDetails.username;

            this.setUser(user);

            return user;
        });
    }

    static setProvider(provider) {
        localStorage.setItem('provider', JSON.stringify(provider));
    }

    static getProvider() {
        return JSON.parse(localStorage.getItem('provider'));
    }

    static setUser(user) {
        this._tempUser = user;
        localStorage.setItem('user', JSON.stringify(user));
    }

    /**
     * @public
     */
    static getUser() {
        let result = localStorage.getItem('user');

        if (result) {
            result = JSON.parse(result);

            // TODO: Convert to User instance.
            result.expiresAt = new Date(result.expiresAt);

            return result;
        }

        if (this._tempUser) return this._tempUser;

        return null;
    }

    /**
     * @private
     * @return {Promise<Boolean>}
     */
    static _refreshAccessToken() {
        let cred = this.getUser();

        const handleInvalidAccessToken = () => {
            if (this.getUser()) {
                // Should not be logged in.
                this.logOut();
            }
        }

        if (!cred.refreshToken) {
            handleInvalidAccessToken();

            console.warn('Missing refresh token');

            return Promise.resolve(false);
        }

        return this._SERVICE.invoke('refreshAccessToken', [cred], {}, /* unsafe */ true)
            .then(result => {
                if (!result) {
                    console.warn('Failed to refresh access token.');

                    handleInvalidAccessToken();

                    return Promise.resolve(false);
                }

                this.setUser(Object.assign({}, cred, result));
                
                console.log('Access token refreshed.');

                return Promise.resolve(true);
            }).catch(err => {
                console.warn(err);
                handleInvalidAccessToken();

                return Promise.resolve(false);
            });
    }

    /**
     * @public
     */
    static isLoggedIn() {
        if (this._refreshPromise) {
            return this._refreshPromise;
        }

        const user = this.getUser();

        if (!user) {
            return Promise.resolve(false);
        }

        if (new Date() > user.expiresAt) {
            this._refreshPromise = this._refreshAccessToken().then(result => {
                this._refreshPromise = null;

                return Boolean(result);
            });
        }

        return Promise.resolve(true);
    }

    /**
     * @public
     */
    static logIn(nextURL) {
        localStorage.setItem('nextURL', nextURL || window.location.href);

        let modal = new AuthenticationProviderSelectionModalComponent()
            .setOnProviderSelected(provider => {
                this.setProvider(provider);

                this._SERVICE.invoke('getAuthorizeURL', [], {
                    provider,
                }, /* unsafe */ true).then(result => {
                    window.location = result;
                });
            });

        Dom.appendTo(document.body, modal.render());
    }

    /**
     * @public
     */
    static logOut() {
        this._clearCookies();

        window.location = '/';
    }

    /**
     * @private
     */
    static _clearCookies() {
        Object.keys(localStorage).forEach(key => {
            if (key !== 'lastCookieOptIn') {
                localStorage.removeItem(key);
            }
        });
    }

    /**
     * @public
     * @return {Promise<Boolean>}
     */
    static canStoreCookies() {
        let timestamp = localStorage.getItem('lastCookieOptIn');

        if (!timestamp) return false;

        timestamp = new Date(timestamp);
        const diff = (new Date()).getTime() - timestamp.getTime();
        
        if (diff > this._REQUIRE_COOKIE_CONSENT_INTERVAL_MS) return false;

        return true;
    }

    /**
     * @public
     */
    static recordCookieOptIn() {
        const timestamp = new Date();
        const user = Authentication.getUser();
        const consentRecord = Object.assign({
            consentDescription: 'Opt-in to cookies',            
        }, user);

        localStorage.setItem('lastCookieOptIn', String(timestamp));
        
        return this._SERVICE.invoke('recordUserConsent', [], {
            user,
            consentRecord,
        });
    }

    /**
     * @public
     */
    static recordCookieOptOut() {
        const user = Authentication.getUser();
        const consentRecord = Object.assign({
            consentDescription: 'Opt-out from cookies',            
        }, user);

        localStorage.clear();
        
        return this._SERVICE.invoke('recordUserConsent', [], {
            user,
            consentRecord,
        });
    }
}

Authentication._refreshPromise = null;
Authentication._tempUser = null;
Authentication._SERVICE = new BackendService('[["authenticationService"]]', 'authentication');
Authentication._REQUIRE_COOKIE_CONSENT_INTERVAL_MS = 365.25 / 12 * 24 * 60 * 60 * 1000;
Authentication.PROVIDER = {
    GOOGLE: 'google',
    LINKEDIN: 'linkedin',
    MOCK: 'mock',
};

window.Authentication = Authentication;