# Authentication
==============

## Installation

### Link up frontend code
```bash
cd src && ln -s ../authentication/src auth && cd ..
```

### Create authentication service
```python
from authentication.services.AuthenticationService import AuthenticationService

class MyAppAuthenticationService(AuthenticationService):

    def _getDBClient(self):
        # TODO


def lambda_handler(event, context):
    return MyAppAuthenticationService.handleEvent(event, context)
```

### Config
Inject key "authenticationService" with value "MyAppAuthenticationService"

Create config file `config/MyAppAuthenticationService.config.json`, add keys eg:

```json
{
    "googleClientId": "",
    "googleClientSecret": "",
    "googleRedirectURI": "/auth",
    
    "linkedinClientId": "",
    "linkedinClientSecret": "",
    "linkedinRedirectURI": "http://localhost:5000/auth"
}
```

### Import frontend code
```html
<import path="/src/auth/Authentication.html">
```

