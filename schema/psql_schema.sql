DROP TABLE IF EXISTS "Users";
CREATE TABLE "Users" (
  "id" SERIAL NOT NULL,
  "username" VARCHAR(128) DEFAULT NULL,
  "oauth2Id" VARCHAR(128) NOT NULL,
  "oauth2Provider" VARCHAR(32) NOT NULL,
  "domain" VARCHAR(128) NOT NULL DEFAULT '',
  "accessToken" TEXT NULL DEFAULT NULL,
  "refreshToken" TEXT NULL DEFAULT NULL,
  "expiresAt" TIMESTAMP DEFAULT NULL,
  "isAdmin" BOOLEAN NOT NULL DEFAULT '0',
  "apiKey" VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY ("oauth2Id", "oauth2Provider"),
  CONSTRAINT UC_Users UNIQUE ("username"),
  CONSTRAINT uniqueApiKey UNIQUE ("apiKey")
);

CREATE INDEX username ON "Users" ("username");
CREATE INDEX id ON "Users" ("id");

DROP TABLE IF EXISTS "StateTokens";
CREATE TABLE "StateTokens" (
  "stateToken" VARCHAR(32) NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY ("stateToken")
);
