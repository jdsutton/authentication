DROP TABLE IF EXISTS Users;
CREATE TABLE `Users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(128) DEFAULT NULL,
  `oauth2Id` VARCHAR(128) NOT NULL,
  `oauth2Provider` VARCHAR(32) NOT NULL,
  `domain` VARCHAR(128) NOT NULL DEFAULT '',
  `accessToken` TEXT NULL DEFAULT NULL,
  `refreshToken` TEXT NULL DEFAULT NULL,
  `expiresAt` DATETIME DEFAULT NULL,
  `isAdmin` TINYINT(1) NOT NULL DEFAULT '0',
  `apiKey` VARCHAR(64) DEFAULT NULL,
  PRIMARY KEY (`oauth2id`, `oauth2Provider`),
  CONSTRAINT UC_Users UNIQUE (username),
  CONSTRAINT uniqueApiKey UNIQUE (apiKey),
  KEY `username` (`username`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS StateTokens;
CREATE TABLE `StateTokens` (
  `stateToken` VARCHAR(32) NOT NULL,
  `createdAt` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`stateToken`)
) ENGINE=InnoDB;
